﻿// See https://aka.ms/new-console-template for more information
// Console.WriteLine("Hello, World!");


class Program
{
    static void Main(string[] args)
    {
        decimal borrow = 0.0M;
        decimal rate = 0.0M;
        decimal years = 0.0M;
        decimal payment = 0.0M;
        decimal totalPay = 0.0M;
        decimal totalInterest = 0.0M;
        float denominator = 0.0F;

        Console.Write("How much are you borrowing? ");
        borrow = Convert.ToDecimal(Console.ReadLine());

        Console.Write("What is your interest rate? ");
        rate = Convert.ToDecimal(Console.ReadLine());

        Console.Write("How long is your loan (in years)? ");
        years = Convert.ToDecimal(Console.ReadLine());

        denominator = (float)Math.Pow(Convert.ToDouble(rate / 12) + 1, -1 * Convert.ToDouble(years * 12));
        payment = borrow * (rate / 12) / Convert.ToDecimal(1 - denominator);
        totalPay = (payment * 12) * years;
        totalInterest  = totalPay - borrow;

        Console.WriteLine($"Your estimated payment is: {payment}");
        Console.WriteLine($"You paid {totalPay} over the life of the loan");
        Console.WriteLine($"Your total interest cost for the loan was {totalInterest}");
    }
}
